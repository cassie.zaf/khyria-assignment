import numpy as np
import time

#Solution A
data = np.ones(shape=(1000, 1000), dtype=np.float64)
start = time.time()
for i in range(1000):
    for j in range(1000):
        data[i][j] *= 1.0000001
        data[i][j] *= 1.0000001
        data[i][j] *= 1.0000001
        data[i][j] *= 1.0000001
        data[i][j] *= 1.0000001
t = time.time()
print("Time elapsed with solution A: {} sec".format(t-start))

#Solution B
data = np.ones(shape=(1000, 1000), dtype=np.float64)

start = time.time()
data = data*(1.0000001**5)
t = time.time()
print("Time elapsed with solution B: {} sec".format(t-start))

