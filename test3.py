import time
import numpy as np
import ipdb

def calculate_result(arr1, arr2):
    # ipdb.set_trace()
    result = [[0 for _ in range(len(arr2[0]))] for _ in range(len(arr1))]
    for i in range(len(arr1)):
        for j in range(len(arr2[0])):
            for k in range(len(arr2)):
                result[i][j] += arr1[i][k] * arr2[k][j]
    return result

def numpy_result(arr1,arr2):
    return( np.matmul(arr1,arr2))


# Example
arr1 = np.random.randint(0,100,(400,200))
arr2 = np.random.randint(0,100,(200,100))

start = time.time()
res = calculate_result(arr1,arr2)
t = time.time()
print("Time elapsed with solution A: {} sec".format(t-start))

start = time.time()
res= numpy_result(arr1,arr2)
t = time.time()
print("Time elapsed with solution B: {} sec".format(t-start))
