import xml.etree.ElementTree as ET
import zipfile
from os import listdir
from os.path import isfile, join
import gdown

def download_unzip_dataset(url: str, output: str):

    gdown.download(url, output, quiet=False)
    # Unzip data
    with zipfile.ZipFile(output, "r") as zip_ref:
        zip_ref.extractall(".")

def get_data_files(files_path: str):
    files = [
        file for file in listdir(files_path)
        if isfile(join(files_path, file)) and file != "truth.txt"
    ]
    return files

def extract_texts_from_files(files: str, files_path: str):
    t_text = []
    for file in files:
        text_doc_1 = [r.text for r in ET.parse(join(files_path, file)).getroot()[0]]
        t_text.append(" ".join(t for t in text_doc_1))
    return t_text

