import utils
import ipdb

def main():
    url="https://drive.google.com/uc?id=1jI1cmxqnwsmC-vbl8dNY6b4aNBtBbKy3"
    output="Twitter.zip"
    path_train="Data/train/en"
    path_test="Data/test/en"
    download = True

    t_train, t_test = load_data(url, output, path_train, path_test, download)

def load_data(url: str, output: str, path_train: str, path_test: str, download: bool):
    ipdb.set_trace()
    #Download data
    if download:
        utils.download_unzip_dataset(url, output)
    # Get train, test data files
    tweets_train_files = utils.get_data_files(path_train)
    tweets_test_files = utils.get_data_files(path_test)
    # Extract texts from each file
    t_train = utils.extract_texts_from_files(tweets_train_files, path_train)
    t_test = utils.extract_texts_from_files(tweets_test_files, path_test)

    return t_train, t_test

if __name__ == "__main__":
    main()
